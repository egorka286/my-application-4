package com.example.myapplication4;

public interface BankObject {
    class gpsPoint {
        public double x;
        public double y;

        public gpsPoint(double x, double y) {
            this.x = x;
            this.y = y;
        }

        public double distance(gpsPoint p) {
            return Math.sqrt(Math.pow((x - p.x), 2) + Math.pow((y - p.y), 2));
        }

        public boolean equals(gpsPoint p) {
            return x == p.x && y == p.y;
        }
    }

    String getAddress();

    gpsPoint getGpsPoint();

    int getTypeId();
}
