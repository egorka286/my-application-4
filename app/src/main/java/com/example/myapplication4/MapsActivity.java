package com.example.myapplication4;

import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;

import com.example.myapplication4.databinding.ActivityMapsBinding;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private static final String baseUrl = "https://belarusbank.by/";
    private static final String city = "Гомель";
    private static final BankObject.gpsPoint startPoint = new BankObject.gpsPoint(52.425163, 31.015039);
    private static AtmApi atmApi;
    private static InfoboxApi infoboxApi;
    private static FilialApi filialApi;
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        com.example.myapplication4.databinding.ActivityMapsBinding binding = ActivityMapsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                .build();
        atmApi = retrofit.create(AtmApi.class);
        infoboxApi = retrofit.create(InfoboxApi.class);
        filialApi = retrofit.create(FilialApi.class);
    }

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        mMap = googleMap;

        LatLng start = new LatLng(startPoint.x, startPoint.y);
        mMap.addMarker(new MarkerOptions().position(start).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));

        Disposable disposable = Observable.concat(atmApi.getData(city), infoboxApi.getData(city), filialApi.getData(city))
                .flatMapIterable(x -> x)
                .toSortedList((a, b) -> {
                    Double distA = a.getGpsPoint().distance(startPoint);
                    Double distB = b.getGpsPoint().distance(startPoint);
                    return distA.compareTo(distB);
                })
                .toObservable()
                .flatMapIterable(x -> x)
                .distinctUntilChanged((a, b) -> a.getGpsPoint().equals(b.getGpsPoint()))
                .take(10)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(a -> {
                    LatLng xy = new LatLng(a.getGpsPoint().x, a.getGpsPoint().y);
                    mMap.addMarker(new MarkerOptions().position(xy).title(a.getAddress() + " (" + getString(a.getTypeId()) + ")"));
                });
    }
}