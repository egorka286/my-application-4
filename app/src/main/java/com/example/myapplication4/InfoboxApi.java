package com.example.myapplication4;

import java.util.List;

import io.reactivex.rxjava3.core.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface InfoboxApi {
    @GET("api/infobox")
    Observable<List<Infobox>> getData(@Query("city") String city);
}
