package com.example.myapplication4;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Atm implements BankObject {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("area")
    @Expose
    private String area;
    @SerializedName("city_type")
    @Expose
    private String cityType;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("address_type")
    @Expose
    private String addressType;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("house")
    @Expose
    private String house;
    @SerializedName("install_place")
    @Expose
    private String installPlace;
    @SerializedName("work_time")
    @Expose
    private String workTime;
    @SerializedName("gps_x")
    @Expose
    private String gpsX;
    @SerializedName("gps_y")
    @Expose
    private String gpsY;
    @SerializedName("install_place_full")
    @Expose
    private String installPlaceFull;
    @SerializedName("work_time_full")
    @Expose
    private String workTimeFull;
    @SerializedName("ATM_type")
    @Expose
    private String aTMType;
    @SerializedName("ATM_error")
    @Expose
    private String aTMError;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("cash_in")
    @Expose
    private String cashIn;
    @SerializedName("ATM_printer")
    @Expose
    private String aTMPrinter;

    @Override
    public String getAddress() {
        return addressType + address + " " + house;
    }

    @Override
    public gpsPoint getGpsPoint() {
        return new gpsPoint(Double.parseDouble(gpsX), Double.parseDouble(gpsY));
    }

    @Override
    public int getTypeId() {
        return R.string.atm;
    }
}