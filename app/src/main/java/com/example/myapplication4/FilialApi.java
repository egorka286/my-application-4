package com.example.myapplication4;

import java.util.List;

import io.reactivex.rxjava3.core.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface FilialApi {
    @GET("api/filials_info")
    Observable<List<Filial>> getData(@Query("city") String city);
}
